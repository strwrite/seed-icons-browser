# Seed Icons Browser

## Roadmap

Add search using keywords provided by collections.

Add support for the case, when custom set of icons is managed 
centrally, Seed Icons Browser in addition to browsing of them
also provides metadata for frontend apps to use during build
of `seed-icons` crate.

