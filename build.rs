// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

#[allow(unused_must_use)]
fn main() -> Result<(), seed_icons_gen::GetResourcesError> {
    seed_icons_gen::get_fa_resources("static")?;

    std::fs::remove_dir_all("static/font-awesome/svgs");
    std::fs::remove_dir_all("static/font-awesome/less");
    std::fs::remove_dir_all("static/font-awesome/scss");
    std::fs::remove_dir_all("static/font-awesome/sprites");
    std::fs::remove_dir_all("static/font-awesome/js");
    std::fs::remove_dir_all("static/font-awesome/metadata");
    Ok(())
}