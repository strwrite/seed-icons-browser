# Copyright (c) 2020 Timur Sultanaev All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

# Add license to all new files
git status -s -uno  | grep -E '^(A|M).*' | awk '{print $2}' | xargs -n 1 addlicense -l bsd -c "Timur Sultanaev"
git status -s -uno  | grep -E '^(A|M).*' | awk '{print $2}' | xargs -n 1 git add 
