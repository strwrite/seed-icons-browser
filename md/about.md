## Seed Framework

[Seed](https://seed-rs.org/) is an awesome Rust framework 
for implementing frontend web applications.

### Seed Icons

Seed Icons is a crate designed to help developers rapidly style their 
applications using one or several collections of icons.

Currenly supported collections:
- [Font Awesome](https://fontawesome.com/)
- [Material Icons](https://material.io/resources/icons/)

Look in the roadmap in [crates.io](https://crates.io/crates/seed-icons) to 
know about plans to add other collections.