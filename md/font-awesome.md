## Font Awesome

[Font Awesome](https://fontawesome.com/) is a widely known icons
collection. 

### Add dependency

In your Cargo.toml:
``` toml
[dependencies.seed-icons]
version = "0.4.0"
```

This version comes with Font Awesome `5.14.0`.


### Static resources

To use the collection with `seed` and `seed-icons`, you'd need 
to get some of the static content, which collection includes in 
addition to adding crate to dependencies.

Here is proposed solution using `build.rs` and `seed-icons-gen`
crate. You need to add `seed-icons-gen` to your build dependencies in
Cargo.toml:

``` toml
[build-dependencies]
seed-icons-gen = "0.2.1"
```

That would get you Font Awesome `5.14.0`.

Now you can write something like this in your `build.rs`:

``` rust
fn main() -> Result<(), seed_icons_gen::GetResourcesError> {
    seed_icons_gen::get_fa_resources("static")?;      
    println!("cargo:rerun-if-changed=static/font-awesome");
    Ok(())
}
```

This will do the following: firstly `seed_icons_gen::get_fa_resources`
would download static resources from official url and put them in "font-awesome"
folder under "static", that you passed. Then println outputs 
directory name with results to cargo, so that build tool would know 
when to run `build.rs` again in case if something changes with
downloaded files.

Finally you can link the static content from `index.html` f.e. like that:

``` html
    <link href="static/font-awesome/css/all.css" rel="stylesheet">
```