## Material Icons

Material icons is a collection of icons, which is a part of a
[material design](http://material.io/). 

### Add dependency and feature

In your Cargo.toml:
``` toml
[dependencies.seed-icons]
version = "0.4.0"
features = [
  "material_icons"
]
```

### Static resources

To use the collection with `seed` and `seed-icons`, 
you'd need to get some of the static content, which collection 
includes in addition to adding `seed-icons` crate to dependencies
and calling generated functions from your code.

The easiest way to get static resources is to add following in 
your `index.html`:

``` html
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
```

Alternatively you could use `seed-icons-gen` crate to download static resources
and place it inside of your file system. 
However, that approach has a limitation, 
that only so called `regular` icons would be available, as resources available
officially only include `MaterialIcons-Regular.woff`. 
This means that about 15 percent of icons won't be available unfortunately.

This way goes like that: firstly you need to add `seed-icons-gen` to your 
build dependencies in Cargo.toml:

``` toml
[build-dependencies]
seed-icons-gen = "0.3.0"
```

Now you can write something like this in your `build.rs`:

``` rust
fn main() -> Result<(), seed_icons_gen::GetResourcesError> {
    seed_icons_gen::get_mi_resources("static")?;
    println!("cargo:rerun-if-changed=static/material-icons");
    Ok(())
}
```

That would get you material-icons `3.0.1`.

This will do the following: firstly `seed_icons_gen::get_mi_resources`
would download static resources from official url and put them in "material-icons"
folder under "static", that you passed. Then println outputs 
directory name with results to cargo, so that build tool would know 
when to run `build.rs` again in case if something changes with
downloaded files.

Finally you can link the static content from `index.html` f.e. like that:

``` html
    <link href="static/material-icons/iconfont/material-icons.css" rel="stylesheet">
```