/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

module.exports = ({ file, options, env }) => {

  return {
    plugins: [
      require("postcss-import"),
      require("tailwindcss")('tailwind.config.js'),
      require("postcss-typed-css-classes")({
        generator: "rust",
        purge: options.mode === "production",
        output_filepath: "src/generated/css_classes.rs",
        content: [
          { path: ['src/**/*.rs'] },
          {
            path: ['static/index.hbs', 'static/templates/**/*.hbs'],
            regex: /class\s*=\s*["'|][^"'|]+["'|]/g,
            mapper: className => {
              return (className.match(/class\s*=\s*["'|]([^"'|]+)["'|]/)[1]).match(/\S+/g)
            },
            escape: true
          }
        ],
      }),
      require("autoprefixer")
    ]
  };
};
