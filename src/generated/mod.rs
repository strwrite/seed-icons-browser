// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// File `css_classes.rs` is (re)created during webpack compilation.
// (see `postcss.config.js`)
pub mod css_classes;
