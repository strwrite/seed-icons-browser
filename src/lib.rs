// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// @TODO: uncomment once https://github.com/rust-lang/rust/issues/54726 stable
//#![rustfmt::skip::macros(class)]

#![allow(
    clippy::used_underscore_binding,
    clippy::non_ascii_literal,
    clippy::enum_glob_use,
    clippy::must_use_candidate,
    clippy::wildcard_imports
)]

mod generated;
mod page;

use std::fmt;

use fixed_vec_deque::FixedVecDeque;
use generated::css_classes::C;
use seed::{prelude::*, *};
use seed_hooks::*;

use Visibility::*;

const TITLE_SUFFIX: &str = "Seed Icons Browser";
const USER_AGENT_FOR_PRERENDERING: &str = "ReactSnap";
const STATIC_PATH: &str = "static";
const IMAGES_PATH: &str = "static/images";

const ABOUT: &str = "about";
const BROWSER: &str = "browser";

// ------ ------
//     Init
// ------ ------

fn init(url: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders
        .subscribe(Msg::UrlChanged)
        .stream(streams::window_event(Ev::Scroll, |_| Msg::Scrolled));

    Model {
        base_url: url.to_base_url(),
        page: Page::init(url),
        scroll_history: ScrollHistory::new(),
        menu_visibility: Hidden,
        in_prerendering: is_in_prerendering(),
        collections: seed_icons::collections(),
        drawer_open: false,
        clicked: None,
    }
}

fn is_in_prerendering() -> bool {
    let user_agent =
        window().navigator().user_agent().expect("cannot get user agent");

    user_agent == USER_AGENT_FOR_PRERENDERING
}

// ------ ------
//     Model
// ------ ------

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Visibility {
    Visible,
    Hidden,
}

impl Visibility {
    pub fn toggle(&mut self) {
        *self = match self {
            Visible => Hidden,
            Hidden => Visible,
        }
    }
}

// We need at least 3 last values to detect scroll direction,
// because neighboring ones are sometimes equal.
type ScrollHistory = FixedVecDeque<[i32; 3]>;

#[derive(Clone, Eq, PartialEq)]
pub struct ClickedIcon {
    pub name: String,
    pub collection: String,
    pub full_module_name: String,
}

pub struct Model {
    pub base_url: Url,
    pub page: Page,
    pub scroll_history: ScrollHistory,
    pub menu_visibility: Visibility,
    pub in_prerendering: bool,
    pub collections: Vec<seed_icons::IconCollection<'static, Msg>>,
    pub drawer_open: bool,
    pub clicked: Option<ClickedIcon>
}

// ------ Page ------

#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum Page {
    Browser,
    About,
    NotFound,
}

impl fmt::Display for Page {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Page {
    pub fn init(mut url: Url) -> Self {
        // log!("{:?}", url);
        // let hash_path = url.hash_path();
        // log!("{:?}", hash_path);
        // let hash_path: Vec<&str> = hash_path.into_iter().map(|s|s.as_str()).collect();
        // log!("{:?}", hash_path);
        let (page, title) = match url.remaining_hash_path_parts().as_slice() {
            [BROWSER] => (Self::Browser, TITLE_SUFFIX.to_owned()),
            // [] => (Self::Browser, TITLE_SUFFIX.to_owned()),
            [ABOUT] | [] => (Self::About, format!("About - {}", TITLE_SUFFIX)),
            _ => (Self::NotFound, format!("404 - {}", TITLE_SUFFIX)),
        };
        document().set_title(&title);
        page
    }
}

// ------ ------
//     Urls
// ------ ------

struct_urls!();
impl<'a> Urls<'a> {
    pub fn home(self) -> Url {
        self.base_url()
    }

    pub fn about(self) -> Url {
        self.base_url().add_path_part(ABOUT)
    }
}

// ------ ------
//    Update
// ------ ------

pub enum Msg {
    UrlChanged(subs::UrlChanged),
    ScrollToTop,
    Scrolled,
    ToggleMenu,
    HideMenu,
    IconClicked {name: String, collection: String, full_module_name: String},
    DrawerClosing,
}

pub fn update(msg: Msg, model: &mut Model, _: &mut impl Orders<Msg>) {
    match msg {
        Msg::DrawerClosing => {
            model.drawer_open = false;
        },
        Msg::IconClicked { name, collection, full_module_name } => {
            // clicking in the same icon as current open one would close the drawer
            match &model.clicked {
                Some(icon) if icon.full_module_name == full_module_name 
                                         && model.drawer_open => {
                    model.drawer_open = false;
                    return;
                },
                _ => (),
            }
            model.drawer_open = true;
            model.clicked = Some(ClickedIcon{
                name: name,
                collection: collection,
                full_module_name: full_module_name,
            });
        },
        Msg::UrlChanged(subs::UrlChanged(url)) => {
            model.page = Page::init(url);
            model.drawer_open = match model.page {
                Page::Browser => model.drawer_open,
                _ => false,
            }
        },
        Msg::ScrollToTop => window().scroll_to_with_scroll_to_options(
            web_sys::ScrollToOptions::new().top(0.),
        ),
        Msg::Scrolled => {
            // Some browsers use `document.body.scrollTop`
            // and other ones `document.documentElement.scrollTop`.
            let mut position = body().scroll_top();
            if position == 0 {
                position = document()
                    .document_element()
                    .expect("get document element")
                    .scroll_top()
            }
            *model.scroll_history.push_back() = position;
        },
        Msg::ToggleMenu => model.menu_visibility.toggle(),
        Msg::HideMenu => {
            model.menu_visibility = Hidden;
        },
    }
}

// ------ ------
//     View
// ------ ------

// Notes:
// - \u{00A0} is the non-breaking space
//   - https://codepoints.net/U+00A0
//
// - "▶\u{fe0e}" - \u{fe0e} is the variation selector, it prevents ▶ to change to emoji in some browsers
//   - https://codepoints.net/U+FE0E
#[topo::nested]
pub fn view(model: &Model) -> impl IntoNodes<Msg> {
    div![
        C![
            IF!(not(model.in_prerendering) => C.fade_in),
            C.flex,
            C.flex_col,
        ],
        div![
            C![
                C.flex,
                C.flex_col,
                C.md__flex_row,
                C.overflow_x_scroll,
                C.sm__text_20,
                C.md__text_25,
                C.min_h_screen,
            ],
            div![
                C![
                    C.flex,
                    C.flex_col,
                    C.md__flex_grow,
                ],
                page::partial::header::view(model),
                match model.page {
                    Page::Browser => page::home::view(model),
                    Page::About => page::about::view(),
                    Page::NotFound => page::not_found::view(),
                },
            ],
            page::partial::drawer::view(model),
        ],
        page::partial::footer::view(),
    ]
}

pub fn image_src(image: &str) -> String {
    format!("{}/{}", IMAGES_PATH, image)
}

pub fn asset_path(asset: &str) -> String {
    format!("{}/{}", STATIC_PATH, asset)
}

// ------ ------
//     Start
// ------ ------

#[wasm_bindgen(start)]
pub fn run() {
    log!("Starting app...");

    App::start("app", init, update, view);

    log!("App started.");
}
