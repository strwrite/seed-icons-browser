// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::{
    generated::css_classes::C, Msg
};
use seed::{prelude::*, *};
use crate::page::partial::centered;

#[allow(clippy::too_many_lines)]
pub fn view() -> Node<Msg> {
    main![
        C![
            C.overflow_x_scroll,
        ],
        centered::view(
            div![
                C![
                    C.about,
                    C.px_5,
                ],
                md!(include_str!("../../md/about.md")),
                md!(include_str!("../../md/font-awesome.md")),
                md!(include_str!("../../md/material-icons.md"))
            ]
        )
    ]
}

