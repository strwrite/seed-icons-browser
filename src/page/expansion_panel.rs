// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use seed::{prelude::*, *};
use seed_hooks::*;

pub struct ExpantionPanelProperties<T> {
    pub title: String,
    pub content: Node<T>,
}

#[topo::nested]
pub fn view<T: 'static>(panel: ExpantionPanelProperties<T>) -> Node<T> {
    let show = use_state(|| true);
    let title_margin_style = if show.get() { "20px" } else { "12px" };
    let icon_rotate_style = if show.get() {"rotate(180deg)"} else {""};
    let content_height_style = if show.get() {"auto"} else {"0px"};
    let content_overflow_style = if show.get() {"inherit"} else {"hidden"};
    let margin_bottom = if show.get() {"16px"} else {"0"};
    div![
        
        style! [
            "margin" => format!("0 0 {} 0", margin_bottom),
            "transition" => "margin 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
            "border-top-left-radius" => "4px",
            "border-top-right-radius" => "4px",
            "box-shadow" => "0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)",
        ],
        div![
            style![
                "justify-content" => "center",
                "display" => "flex",
                "position" => "relative",
            ],
            attrs! {
                "role" => "button",
            },
            div![
                style! [
                    "padding" => "0 24px 0 24px",
                    "transition" => "margin 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                    "display" => "flex",
                    "flex-grow" => "1",
                    "margin" => format!("{} 0", title_margin_style),
                ],
                p![
                    style! [
                        "padding-right" => "32px",
                    ],
                    panel.title
                ]
            ],
            div![
                style! [
                    "transform" => format!("translateY(-50%) {}", icon_rotate_style),
                    "display" => "inline-flex",
                    "position" => "absolute",
                    "right" => "8px",
                    "top" => "50%",
                    "transition" => "transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                ],
                span! [
                    style! [
                        "padding" => "12px",
                    ],
                    svg![
                        style! [
                            "width" => "1em",
                            "height" => "1em",
                        ],
                        path![
                            attrs! {
                                At::D => "M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"
                            }
                        ]
                    ]
                ]
            ],
            show.on_click(|showing| {
                *showing = !*showing
            }),
        ],
        div![
            style! [
                "min-height" => "0px",
                "transition-duration" => "208ms",
                "height" => content_height_style,
                "overflow" => content_overflow_style,
            ],
            panel.content
        ]
    ]
}
