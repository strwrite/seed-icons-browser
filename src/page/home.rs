// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::{Model, page::expansion_panel};
use crate::{generated::css_classes::C, Msg};
use seed::{prelude::*, *};

fn view_icon(
    icon: &seed_icons::RegisteredIcon<'static, Msg>, 
    collection_name: String,
    drawer_open: bool) -> Node<Msg> {
    let view = icon.view_with_classes;
    let name = icon.name.clone();
    let full_module_name = icon.full_module_name.clone();
    let collection = collection_name;
    let grid = if drawer_open {
        C![
            // Grid when drawer is open: 3 icons in a row for middle devices
            // , 6 for large, 12 for very large
            C.md__w_1of3,
            C.lg__w_1of6,
            C.xl__w_1of9,
        ]
    } else { 
        C![
            // Grid for default: 3 icons in a row for small devices
            // , 6 for middle, 9 large, 12 for very large
            C.w_1of3,
            C.md__w_1of6,
            C.lg__w_1of9,
            C.xl__w_1of12,
        ]
    };
    li![
        ev(Ev::Click, |_| Msg::IconClicked{
            name: name, 
            collection: collection,
            full_module_name: full_module_name,
        }),
        C![
            C.flex,
            C.flex_col,

            C.my_1,
            C.py_1,
            C.px_2,

            C.border_opacity_0,
            C.hover__border_opacity_40,
            C.border_r_2,
            C.border_l_2,
            C.rounded_md,
            C.border_gray_9,
            C.cursor_pointer,
            C.text_gray_6,
            C.hover__text_gray_9,
        ],
        grid,
        div![
            C![ 
                C.text_center,
                C.text_gray_8,
                C.hover__text_gray_9,
            ],
            view(vec![ C.text_45 ])
        ],
        p![
            C![
                C.text_center,
                C.text_0_8,
                C.overflow_x_hidden,
            ],
            icon.name.clone(),
        ],
    ]
}

fn view_icons(icons: &Vec<seed_icons::RegisteredIcon<'static, Msg>>, collection_name: String, drawer_open: bool) -> Vec<Node<Msg>> {
    icons.into_iter().map(|icon| 
        view_icon(icon, collection_name.clone(), drawer_open)
    ).collect()
}

fn view_collection(collection: &seed_icons::IconCollection<'static, Msg>, drawer_open: bool) -> Node<Msg> {
    let name = &collection.name;
    // don't pad right side if drawer is shown, as it has it's own padding
    let padding = if drawer_open { C![C.pl_6] } else { C![C.px_6] };
    div![
        padding,
        expansion_panel::view(expansion_panel::ExpantionPanelProperties {
            title: name.clone(),
            content: ul![
                C![
                    C.flex,
                    C.flex_row,
                    C.flex_wrap,
                ],
                view_icons(&collection.icons, name.clone(), drawer_open)            
            ],
        })
    ]

}


pub fn view(model: &Model) -> Node<Msg> {
    if model.in_prerendering {
        return div![]
    }
    let drawer_open_classes = if model.drawer_open {
        Some(C![
            C.hidden, // hide until middle sizes if drawer is open
            C.md__block, 
            C.mr_570px, // gives drawer a space
        ])
    } else {
        None
    };
    main![
        drawer_open_classes,
        (&model.collections).into_iter()
            .map(|collection| { 
                view_collection(collection, model.drawer_open) 
        }),
    ]
}
