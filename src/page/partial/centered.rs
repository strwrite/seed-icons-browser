use seed::{prelude::*, *};

use crate::{
    generated::css_classes::C, Msg
};

pub fn view(content: Node<Msg>) -> Node<Msg> {
    div![
        C![
            C.w_full,
        ],
        div![
            style![
                "margin" => "0 auto",
            ],
            C![
                C.container,
            ],
            content
        ]
    ]
}