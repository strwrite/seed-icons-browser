use crate::{Model, Msg, generated::css_classes::C};
use seed::{prelude::*, *};

use super::code_block;

pub fn view(model: &Model) -> Node<Msg> {
    if !model.drawer_open {
        return div![]
    }
    match &model.clicked {
        Some(icon) => view_icon(icon),
        None =>  div![]
    }
}

fn view_icon(icon: &crate::ClickedIcon) -> Node<Msg> { 
    section![
        C![
            C.fixed,
            C.right_0,
            C.top_0,
            C.bottom_0,
            C.w_full,
            C.md__w_520px,
            C.bg_gray_1,
            C.z_10,
            C.overflow_scroll,
        ],
        button![
            ev(Ev::Click, |_| Msg::DrawerClosing),
            style!{ "right" => "20px", "top" => "20px" },
            C![
                C.absolute
            ], ">"
        ],
        div![
            C![
                C.p_8,
            ],
            p![
                format!("Name: {}", icon.name)
            ],
            div![
                C![
                    C.text_0_8,
                ],
                format!("Set env to build only this icon: SEED_ICONS_FILTER_NAMES={}", icon.name)
            ],
            p![
                C![
                    C.mt_8,
                ],
                format!("Collection: {}", icon.collection)
            ],
            div![
                C![
                    C.text_0_8,
                ],
                "Follow ", 
                a![ attrs!{At::Href => "#/about"}, "instructions"], 
                " to add resources to your application."
            ],
            p![
                C![
                    C.mt_8,
                ],
                "Usage:"
            ],
            code_block::view(
                "rust",
                format!(r#"
// just icon
{}::i()
// with classes
{}::i_c(
    vec!["class1", "class2"]
)"#,     icon.full_module_name, &icon.full_module_name).trim(),
                C.text_1
            )

        ]
    ]
}