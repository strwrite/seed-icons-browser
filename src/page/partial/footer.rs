// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::{generated::css_classes::C, Msg};
use seed::{prelude::*, *};

pub fn view() -> Node<Msg> {
    footer![
        C![
            C.w_full,
            C.border_gray_2,
            C.border_t_2,
            C.mt_4
        ],
        div![
            style![
                "margin" => "0 auto",
            ],
            C![
                C.container,
                C.p_5,
                C.text_1,
                C.text_gray_6,
                C.text_center,                
            ],
            "This site is powered by awesome ",
            span![a![
                C![
                    C.text_gray_5,
                ],
                "Seed",
                attrs! {
                    At::Href => "https://seed-rs.org/",
                }
            ]]
        ]
    ]
}
