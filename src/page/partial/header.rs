// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::{
    generated::css_classes::C,
    Model, Msg,
};
use seed::{prelude::*, *};

fn count_icons (collections: &Vec<seed_icons::IconCollection<'static, Msg>>) -> usize {
    collections.into_iter().map(|collection| {
        (&collection.icons).into_iter().count()
    }).sum()
}

fn link(name: &str, href: &str, current_page: &String) -> Node<Msg>{
    let active = *current_page == name.to_string();
    li![
        a![
            attrs!{ At::Href => href }, 
            h2![
                C![
                    C.py_1,
                    C.px_2,
                    C.hover__underline,
                    C.font_bold,
                    C.text_blue_9,
                    C.hover__text_blue_7,
                    if active { C.border_t_4 } else { C.border_t_0 },
                    if active { C.border_b_4 } else { C.border_b_0 },
                ],
                name
            ]
        ]
    ]
}

#[allow(clippy::too_many_lines)]
fn content(model: &Model) -> Node<Msg> {
    let icons_num:usize = count_icons(&model.collections);
    let collections_num = model.collections.len();
    let totals_line = format!("{} icons in {} collections", icons_num, collections_num);
    let page = model.page.to_string();
    div![
        C![
            C.p_5,
            C.flex,
            C.items_center,
        ],
        span! [
            C![
                C.w_16,
                C.md__w_20,
            ],
            img![
                attrs!{
                    At::Src => "static/images/logo.svg"
                }
            ],
        ],
        div![
            C![
                C.pl_8,
            ],
            h1![
                C![
                    C.text_1_5,
                    C.md__text_2,
                ],
                a![
                    attrs!{ At::Href => "#" }, 
                    C![
                        C.flex,
                        C.flex_col,
                        C.md__flex_row,
                        C.items_center,
                        C.justify_start,
                        C.whitespace_no_wrap,
                    ],
                    "Seed Icons",
                    span![           
                        C![
                            C.text_gray_4,
                            C.text_0_8,
                            C.md__text_1,
                            C.pl_2,
                            C.hidden,
                            C.lg__block
                        ],
                        totals_line
                    ]
                ]
            ],
            nav![
                ul![
                    C![
                        C.flex,
                        C.flex_row,
                        C.text_1,
                        C.md__text_1_5,
                        C.rounded_br_28px,
                        C.bg_blue_2,
                    ],
                    link("About", "#/about", &page),
                    link("Browser", "#/browser", &page),
                ]
            ]
        ]
        
    ]
}

pub fn view(model: &Model) -> Node<Msg> {
    header![
        content(model)
    ]
}
