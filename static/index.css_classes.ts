/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

/**
 * This file is used when `yarn generate:css_classes` is called.
 * We want to run only pipeline that processes CSS so it generates `css_classes.rs`.
 */
import "../css/styles.css";

