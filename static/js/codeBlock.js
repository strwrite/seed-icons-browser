/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

import { LitElement, html, css } from 'https://unpkg.com/lit-element/lit-element.js?module';
import { unsafeHTML } from 'https://unpkg.com/lit-html/directives/unsafe-html.js?module';

class CodeBlockElement extends LitElement {
    static get properties() {
        return {
            lang: "",
            code: "",
            classname: "",
        };
    }

    render() {
        const highlightedCode = highlightCode(this.code, this.lang);
        if (this.classname === "inlinecode") {
            return html`<code class='hljs ${this.classname}'>${unsafeHTML(highlightedCode)}</code>`;
        }
        return html`<pre><code class='hljs ${this.classname}'>${unsafeHTML(highlightedCode)}</code></pre>`;
    }

    createRenderRoot() {
        return this;
    }
}
customElements.define('code-block', CodeBlockElement);

function highlightCode(code, lang) {
    const highlightedCode =
        window
            .hljs
            .highlightAuto(code, lang ? [lang] : undefined)
            .value;
    return highlightedCode
}
